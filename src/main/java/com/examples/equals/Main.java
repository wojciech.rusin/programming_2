package com.examples.equals;

import com.examples.equals.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        new User("name", "sur", 1 );
        List<User> users = new ArrayList<>();
        users.add(new User("name", "surname",1));
        users.add(new User("name", "surname",1));
        users.add(new User("name", "surname",1));
        users.add(new User("name", "surname",1));
        users.add(new User("name", "surname",1));
        users.add(new User("name", "surname",1));

        System.out.println("array size: " + users.size());

        Set<User> noDuplicates = new HashSet<>(users);

        System.out.println("set size: " + noDuplicates.size());



    }
}
/* to do:
* hashCode taki sam, ale equals ma zwrócić true*/