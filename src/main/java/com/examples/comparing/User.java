package com.examples.comparing;

public class User implements Comparable<User> {
    private String name;
    private String surname;

    private int height;

    public User(String name, String surname, Integer height) {
        this.name = name;
        this.surname = surname;
        this.height = height;

    }

    public int getHeight() {
        return height;
    }

    @Override
    public int compareTo(User user) {
        int bySurname = surname.compareTo(user.surname);
        return bySurname ==0 ? name.compareTo(user.name) : bySurname; //skrócony zapis pętli if
        // warunek if ? co kiedy prawdziwy warunek : co kiedy nie prawdziwy warunek
//        if(bySurname == 0)
//            return name.compareTo(user.name);
//        public static Comparator<User> BY_HEIGHT = new Comparator<User>() {
//
////            @Override
////            public int compare(User o1, User o2) {return o1.getHeight() - o2.getHeight();
////            }
//        }


    }
}
