package com.examples.shop;

import java.util.*;

public class ShopDao {

    private Map<Category, List<Product>> db = new HashMap<>(); // mapa kategorii na listę produktów

    void addCategory(Category category) {db.put(category, new ArrayList<>());/* dodanie kategori */}

    void removeCategory(Category category) {  db.remove(category);/*usunięcie kategorii*/    }

    void addProduct(Category c, Product p) { db.get(c).add(p);/* dodanie produktu do kategorii */ }

    void removeProduct(Category c, Product p) {db.get(c).remove(p);/*usunięcie produktu z danej kategorii */}

    List<Product> getProducts(Category c) { return db.get(c);/* pobranie listy produktów z kategorii*/ }

    Set<Category> allCategories(){ return db.keySet();/*pobieranie wszystkich produktów*/    }

    public void addProduct(Product product) {
    }


}



