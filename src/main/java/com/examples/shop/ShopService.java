package com.examples.shop;

import com.sun.scenario.effect.impl.prism.PrDrawable;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class ShopService {

    private final ShopDao dao;

    public ShopService(ShopDao dao) {
        this.dao = dao;
    }

    public void addCategory(String name) {
        dao.addCategory(new Category(name));
    }

    public Collection<Category> findAllCategories() {
        return dao.allCategories();
    }

    public Collection<Product> findAllProducts() {
        dao.allCategories().stream()
                .map(c -> dao.getProducts(c))
                .flatMap(products -> products.stream())
                .collect(toList());

        // Alternatywne rozwiązania
//        Collection<Product> productsAlternative = new HashSet<>();
//        for (Category c : dao.allCategories())
//            productsAlternative.addAll(dao.getProducts(c));
//
//        // dao.allCategories().forEach(c->);
//        return productsAlternative;
        return findAllProducts();
    }

    private Product findByName(String name){
        return find((t)->t.getName().equals(name));
    }

    private Product findBySerial(int serial){
       return find((t) -> t.getSerial() == serial);
    }

    private Product find(Predicate<Product> p){
        findAllProducts().stream()
                .filter(product -> p.test(product))
                .findFirst().get();// pobiera optional, czyli musimy wyciągnąć go get


        return null;
    }

    public void removeProductByName(String name) {
        removeProduct(p -> p.getName().equals(name));
//        Alternatywne
//        for(Category c : dao.allCategories())
//            dao.getProducts(c).removeIf(p -> p.getName().equals(name));
    }

    public void removeProductBySerial(int serial) {
        removeProduct(p -> p.getSerial() == serial);
        //Alternatywne rozw
//        for(Category c : dao.allCategories())
//            dao.getProducts(c).removeIf(p -> p.getSerial() == serial);
//        Alternatwne
//        dao.allCategories()
//                .forEach(c -> dao.getProducts(c).removeIf(p -> p.getSerial() == serial));
    }
    private void removeProduct(Predicate<Product> p){
        dao.allCategories().forEach(c -> dao.getProducts(c).removeIf(p)); // wytłumaczone później
    }

    public void addProduct(String name, String category) {
        dao.addProduct(new Product(name, category));

    }

    public void removeCategory(String name) {
        if(!isCategoryEmpty(name))
            throw new RuntimeException("Category is no empty");
        dao.removeCategory(new Category(name));

    }

    private boolean isCategoryEmpty(String name) {
        return false;
    }
}
