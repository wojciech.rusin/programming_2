package com.examples.shop;

import java.lang.reflect.Field;

public class Main {
    public static void main (String[] args) {
        ShopService service = new ShopService(new ShopDao());
        //dla chetnych (konsola lub swing)
        //new Gui(service).start();

        Field[] declaredFields = ShopDao.class.getDeclaredFields();

        for(Field f : declaredFields){
            System.out.println(f);
        }
    }
}
