package com.examples.shop;

public class Product {
    private String name;
    private int serial;

    public Product(String s, String name) {
        this.name = name;
    }

    public Product(String name, int serial) {
        this.serial = serial;
    }

    public String getName() {
        return name;
    }

    public int getSerial() {
        return serial;
    }
}
