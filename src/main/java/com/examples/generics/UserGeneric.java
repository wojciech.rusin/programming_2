package com.examples.generics;

public class UserGeneric <E, K, T> {
    E pesel;
    K name;
    K surname;
    T password;

    public UserGeneric(E pesel, T password){
        this.pesel = pesel;
        this.password = password;
    }

    public void setPassword(T password) {this.password = password;}

    public E getPesel() {
        return pesel;
    }

    public K getName() {
        return name;
    }
    public K getSurname() {return surname;}
}
