package com.examples.generics;


import java.util.Arrays;

import static java.util.Arrays.copyOf;

@SuppressWarnings("unchecked")
public class MyArrayList<T> {
    private Object[] elements = new Object[10];
    private int size = 0;

    public void add(T t) {
        ensureCapacity();
        //dodawanie
        size++;
    }

    public T get(int i) {
//        if(i> elements.length -1 || i < 0 || elements[i] == null)
        if (i > size || i < 0)
            throw new RuntimeException("Out of bound");
        return (T) elements[i];
    }

    public int getSize() {
        return size;
    }

    public void remove(int i) {
        elements[i] = null;
        // TODO sprawdzenie czy i nie wykracza poza tablice
        // TODO powstała dziura w tablicy - przesunąć elementy

    }

    public void remove(T t) {
        remove(getIndex(t));
    }

    public int getIndex(T t) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].equals(t))
                return i;
            throw new RuntimeException("Element not found");
        }
        return 0;
    }

    private void ensureCapacity() {
        if(size == elements.length){
        }
    }
}

